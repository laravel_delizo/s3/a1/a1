<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\POSTController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', [POSTController::class, 'welcome']);

//route for viewing the create posts page
Route::get('/posts/create', [POSTController::class, 'create']);

//route for sending form data via POST request to the /posts endpoint
Route::post('/posts', [postController::class, 'store']);

//route to return a view containing all posts
Route::get('/posts', [POSTController::class, 'index']);

Route::get('/posts/myPosts', [POSTController::class, 'myPosts']);

Route::get('/posts/{id}', [POSTController::class, 'show']);

Route::get('/posts/{id}/edit', [POSTController::class, 'editPost']);

Route::put('/posts/{id}', [POSTController::class, 'update']);

Route::delete('/posts/{id}', [POSTController::class, 'archive']);

/* Route::put('/posts/{id}', [POSTController::class, 'archive']); */

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
