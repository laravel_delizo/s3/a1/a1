<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\POST;

class POSTController extends Controller
{
    //
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if (Auth::user()) {
            //create a new Post object from the post model 
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as the user id

            $post->user_id = Auth::user()->id;
            //save the $post object to the database;
            $post->save();

            return "<p>Post created successfully</p>";
        } else {
            //redirect the user to the login page if not logged in
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from database
        $posts = Post::all();


        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {

        $posts = Post::all()
            ->random(3)
            ->limit(3);

        return view('welcome')->with('posts', $posts);
    }


    public function myPosts()
    {
        if (Auth::user()) {

            $posts = Auth::user()->post;
            /* 
            echo print_r($posts); */

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function editPost($id)
    {
        if (Auth::user()) {
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        } else {
            return redirect('/login');
        }
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }

        return redirect('/posts');
    }


    public function archive(Request $request, $id)
    {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->active = true;
            $post->save();
        }

        return redirect('/posts');
    }
}
