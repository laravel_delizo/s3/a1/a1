@extends('layouts.app')

@section('content')

@if(Auth::user())
@if(Auth::user()->id == $post->user_id)
<form method="POST" action="/posts/{{$post->id}}">

  @method('PUT')
  @csrf

  <div class="form-group">
    <label for="title">Content:</label>
    <input type="text" name="title" id="title" class="form-control" value={{$post->title}}>

    </input>
  </div>
  <div class="form-group">
    <label for="content">Content:</label>
    <textarea name="content" id="content" rows="3" class="form-control">
    {{$post->content}}
    </textarea>
  </div>
  <div class="mt-2">
    <button type="submit" class="btn btn-primary">Update Post</button>
  </div>
</form>

@endif
@endif
@endsection
