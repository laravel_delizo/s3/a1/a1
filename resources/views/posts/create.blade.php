@extends('layouts.app')

@section('content')
<form method="POST" action="/posts">
  @csrf


  <div class="form-group">
    <label for="title">Content:</label>
    <input type="text" name="title" id="title" class="form-control">
    </input>
  </div>
  <div class="form-group">
    <label for="content">Content:</label>
    <textarea name="content" id="content" rows="3" class="form-control"></textarea>
  </div>
  <div class="mt-2">
    <button type="submit" class="btn btn-primary">Create Post</button>
  </div>
</form>
@endsection